# Wirkung

 Wir haben die Schriftart der Überschrift geändert.
```CSS
.headline{
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
}
```
Jetzt müssen die Bilder noch eine einheitliche Größe bekommen.
```CSS
img{
    width: 400px;
    margin: 10px;
}
```
Außerdem sollen sich die Bilder der Größe des Bildschirmes anpassen.
```CSS
.container{
    display:flex;
    flex-wrap:wrap;
    margin:20px
}
```